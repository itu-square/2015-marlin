# Critical patches that should be propagated
#8a5eaa3c9b53548337306f34077d27aed0a69391; Sun, 19 Jan 2014 19:11:51 -0800; 2014-01-20T03:11:51Z
SELECT * FROM marlin_icsme.repository WHERE created_at < TIMESTAMP('2014-01-20T03:11:51Z') AND active_fork = 1 AND id != 1 LIMIT 10000;
SELECT * FROM marlin_icsme.repository WHERE created_at < TIMESTAMP('2014-01-20T03:11:51Z') 
	AND (id) IN (SELECT repo_id FROM branches WHERE (id) 
			 IN (SELECT branch_id FROM branches_commits WHERE commit_id=(SELECT id FROM commits WHERE sha="8a5eaa3c9b53548337306f34077d27aed0a69391"))) AND id !=1;

#6e433985094874f3c3fbf70becd577b8ea2115eb; Patch time: Wed, 12 Feb 2014 13:01:19 -0800; 2014-02-12T21:01:19Z UTC Time
SELECT * FROM marlin_icsme.repository WHERE created_at < TIMESTAMP('2014-02-12T21:01:19Z') AND active_fork = 1 AND id != 1 LIMIT 10000;
SELECT * FROM marlin_icsme.repository WHERE created_at < TIMESTAMP('2014-02-12T21:01:19Z') 
	AND (id) IN (SELECT repo_id FROM branches WHERE (id) 
			 IN (SELECT branch_id FROM branches_commits WHERE commit_id=(SELECT id FROM commits WHERE sha="6e433985094874f3c3fbf70becd577b8ea2115eb"))) AND id !=1;

#aeaf9b9312fb1eff68ed0ecbf42aca07cb9360f1; Sun, 30 Mar 2014 11:34:36 -0700; 2014-03-30T18:34:36Z
SELECT * FROM marlin_icsme.repository WHERE created_at < TIMESTAMP('2014-03-30T18:34:36Z') AND active_fork = 1 AND id != 1 LIMIT 10000;
SELECT * FROM marlin_icsme.repository WHERE created_at < TIMESTAMP('2014-03-30T18:34:36Z') 
	AND (id) IN (SELECT repo_id FROM branches WHERE (id) 
			 IN (SELECT branch_id FROM branches_commits WHERE commit_id=(SELECT id FROM commits WHERE sha="aeaf9b9312fb1eff68ed0ecbf42aca07cb9360f1"))) AND id !=1;

#875950831991f17f8d3ad0fb96ef63d77d5f03f9; Tue, 1 Apr 2014 09:26:19 +0800; 2014-04-01T01:26:19Z
SELECT * FROM marlin_icsme.repository WHERE created_at < TIMESTAMP('2014-04-01T01:26:19Z') AND active_fork = 1 AND id != 1 LIMIT 10000;
SELECT * FROM marlin_icsme.repository WHERE created_at < TIMESTAMP('2014-04-01T01:26:19Z') 
	AND (id) IN (SELECT repo_id FROM branches WHERE (id) 
			 IN (SELECT branch_id FROM branches_commits WHERE commit_id=(SELECT id FROM commits WHERE sha="875950831991f17f8d3ad0fb96ef63d77d5f03f9"))) AND id !=1;


#9db9842aeabe76f78bfda67fac89b75a531bb0b7; Wed, 14 May 2014 21:59:48 +0200; 2014-05-14T19:59:48Z
SELECT * FROM marlin_icsme.repository WHERE created_at < TIMESTAMP('2014-05-14T19:59:48Z') AND active_fork = 1 AND id != 1 LIMIT 10000;
SELECT * FROM marlin_icsme.repository WHERE created_at < TIMESTAMP('2014-05-14T19:59:48Z') 
	AND (id) IN (SELECT repo_id FROM branches WHERE (id) 
			 IN (SELECT branch_id FROM branches_commits WHERE commit_id=(SELECT id FROM commits WHERE sha="9db9842aeabe76f78bfda67fac89b75a531bb0b7"))) AND id !=1;

#0de826160ef590fbf2576d69299c8c4640fc9d94; Mon, 2 Jun 2014 17:11:32 +0200; 2014-06-02T15:11:32Z
SELECT * FROM marlin_icsme.repository WHERE created_at < TIMESTAMP('2014-06-02T15:11:32Z') AND active_fork = 1 AND id != 1 LIMIT 10000;
SELECT * FROM marlin_icsme.repository WHERE created_at < TIMESTAMP('2014-06-02T15:11:32Z') 
	AND (id) IN (SELECT repo_id FROM branches WHERE (id) IN (SELECT branch_id FROM branches_commits WHERE commit_id=(SELECT id FROM commits WHERE sha="0de826160ef590fbf2576d69299c8c4640fc9d94"))) AND id !=1;

#2d22902d080b08c65ebdf5f8b3f03529ccd58144; Thu, 24 Jul 2014 12:04:02 +0200; 2014-07-24T10:04:02Z
SELECT * FROM marlin_icsme.repository WHERE created_at < TIMESTAMP('2014-07-24T10:04:02Z') AND active_fork = 1 AND id != 1 LIMIT 10000;
SELECT * FROM marlin_icsme.repository WHERE created_at < TIMESTAMP('2014-07-24T10:04:02Z')
	AND (id) IN (SELECT repo_id FROM branches WHERE (id) 
			 IN (SELECT branch_id FROM branches_commits WHERE commit_id=(SELECT id FROM commits WHERE sha="2d22902d080b08c65ebdf5f8b3f03529ccd58144"))) AND id !=1;

#62db9848d3f9ffcfe73e74cc7069fb4fe2b63b64; Sat, 16 Aug 2014 06:50:13 -0500; 2014-08-16T11:50:13Z
SELECT * FROM marlin_icsme.repository WHERE created_at < TIMESTAMP('2014-08-16T11:50:13Z') AND active_fork = 1 AND id != 1 LIMIT 10000;
SELECT * FROM marlin_icsme.repository WHERE created_at < TIMESTAMP('2014-08-16T11:50:13Z') 
	AND (id) IN (SELECT repo_id FROM branches WHERE (id) 
			 IN (SELECT branch_id FROM branches_commits WHERE commit_id=(SELECT id FROM commits WHERE sha="62db9848d3f9ffcfe73e74cc7069fb4fe2b63b64"))) AND id !=1;
