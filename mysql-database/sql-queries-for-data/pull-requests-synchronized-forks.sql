# PREQs from feature-development-forks
SELECT * FROM marlin_icsme.pull_requests WHERE (from_repo_name) IN (SELECT DISTINCT(repo_name) FROM forks_analysis_heuristics WHERE features = 'True') AND repo_name = 'ErikZalm/Marlin';

# PREQs from synchronized feature-development-forks
SELECT * FROM marlin_icsme.pull_requests WHERE (from_repo_name) IN (SELECT DISTINCT(repo_name) FROM forks_analysis_heuristics WHERE (repo_name) IN (SELECT DISTINCT(repo_name) FROM forks_analysis_heuristics WHERE features = 'True') AND sync_with_upstream = 'True') AND repo_name = 'ErikZalm/Marlin';

# PREQs from bug-fixing forks
SELECT * FROM marlin_icsme.pull_requests WHERE (from_repo_name) IN (SELECT DISTINCT(repo_name) FROM forks_analysis_heuristics WHERE bug_fixes = 'True') AND repo_name = 'ErikZalm/Marlin';
# Forks that do bug-fixes and created pull-requests
SELECT DISTINCT(from_repo_name) FROM marlin_icsme.pull_requests WHERE (from_repo_name) IN (SELECT DISTINCT(repo_name) FROM forks_analysis_heuristics WHERE bug_fixes = 'True') AND repo_name = 'ErikZalm/Marlin';
