The database can be imported either using MySQL workbench - it is an self-contained schema with creation queries, so the user should just import it - the schema name is marlin_icsme. 
Or it can be imported using this command
	mysql -u [username] -p -h [localhost] < marlin_icsme.sql
	where [username] must be set to your MySQL username
	     [localhost] is your URL the database server

A couple of queries that can be run on the DB are in the sql-queries-for-data folder
