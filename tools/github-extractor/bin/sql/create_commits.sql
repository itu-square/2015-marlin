create table if not exists commits(
	id INT  PRIMARY KEY auto_increment,
	sha VARCHAR(42),
	repo_id INT,
	repo_name VARCHAR(100),
	author_name VARCHAR(100),
	author_date VARCHAR(30),
	committer_name VARCHAR(100),
	committer_date VARCHAR(30),
	message TEXT,
	UNIQUE KEY `index_sha` (sha)
);
