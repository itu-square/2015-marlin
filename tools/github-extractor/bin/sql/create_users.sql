create table if not exists users(
	id INT PRIMARY KEY auto_increment,
    user_github_id INT,
    user_login VARCHAR(100),
    user_name VARCHAR(100),
    user_email VARCHAR(100),
	created_at VARCHAR(30)
);