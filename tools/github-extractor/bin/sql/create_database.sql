
drop schema if exists repository_forks_test;
create schema repository_forks_test;
use repository_forks_test;


create table if not exists repository(
	id INT  PRIMARY KEY auto_increment,
	github_id INT,
	repo_name VARCHAR(500),
	created_at VARCHAR(500),	
	repo_owner_login VARCHAR(500),
    repo_owner_name VARCHAR(500),
    repo_owner_email VARCHAR(500),
	forks_count INT,
	forked_from VARCHAR(500), #this should be the full_name from repo's json
	active_fork BOOL
);

create table if not exists branches(
	id INT PRIMARY KEY auto_increment,
	branch_name VARCHAR(500),
	repo_id INT,
	nr_commits INT,
	last_commit_sha VARCHAR(500)
);
create table if not exists commits(
	id INT  PRIMARY KEY auto_increment,
	sha VARCHAR(500),
	repo_id INT,
	repo_name VARCHAR(500),
	author_id VARCHAR(500),
	author_name VARCHAR(500),
	author_date VARCHAR(500),
	committer_name VARCHAR(500),
	committer_date VARCHAR(500),
	message VARCHAR(60000)
);

create table if not exists users(
	id INT PRIMARY KEY auto_increment,
    user_github_id INT,
    user_login VARCHAR(500),
    user_name VARCHAR(500),
    user_email VARCHAR(500),
	created_at VARCHAR(500)
);

create table if not exists branches_commits(
	branch_commit_id INT PRIMARY KEY auto_increment,
	commit_id INT,
	branch_id INT,
	CONSTRAINT fk_commit_id_constraint
	FOREIGN KEY fk_commit_id (commit_id)
	REFERENCES commits(id) 
	ON UPDATE CASCADE
	ON DELETE RESTRICT
	
);

ALTER TABLE branches_commits
ADD CONSTRAINT  fk_branch_id_constraint
	FOREIGN KEY fk_branch_id (branch_id)
	REFERENCES branches(id)
	ON DELETE RESTRICT
	ON UPDATE CASCADE;

