-- --------------------------------------------------------------------------------
-- Routine DDL
-- Note: comments before and after the routine body will not be stored by the server
-- --------------------------------------------------------------------------------
DELIMITER $$

CREATE DEFINER=`root`@`localhost` PROCEDURE `forks_alive`()
BEGIN

	DECLARE a,b,c,d INT;
	DECLARE alive INT;
	DECLARE my_last_commit_sha VARCHAR(42);
	DECLARE repo_created_at VARCHAR(32);
	DECLARE project_id INT;
	DECLARE my_committer_date VARCHAR(32);
	
	DECLARE cursor1 CURSOR FOR SELECT id FROM repository WHERE id != 1 LIMIT 100000;
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET b = 1;
	
	
	SET b=0;
	
	OPEN cursor1;
	READ_LOOP : LOOP
	
	FETCH  cursor1 into project_id;
	IF b then
		LEAVE READ_LOOP;
	END IF;
	
	
	# Get repository creation date 
	SELECT created_at INTO repo_created_at FROM repository WHERE id = project_id;
	
	BEGIN 
		DECLARE cursor2 CURSOR FOR SELECT last_commit_sha FROM branches WHERE repo_id=project_id;
		DECLARE CONTINUE HANDLER FOR NOT FOUND SET d = 1;
		SET d=0;
		OPEN cursor2;
		BRANCH_LOOP : LOOP
			FETCH cursor2 INTO my_last_commit_sha;
			IF d THEN
				LEAVE BRANCH_LOOP;
			END IF;
			# Get last commit's date on that branch
			SELECT committer_date into my_committer_date FROM commits WHERE sha = my_last_commit_sha;
			SELECT UNIX_TIMESTAMP(my_committer_date) - UNIX_TIMESTAMP(repo_created_at) INTO alive;
			IF alive > 0 THEN
				#UPDATE THE TABLE
				UPDATE repository SET active_fork = TRUE WHERE id = project_id;
				SET d=1;
			END IF;
				
			
		END LOOP;
	END;
	END LOOP;

END