Information:
-ErikZalm-Marlin is the main Marlin repository that we used in our anaysis
-list of forks contains all the active forks and their creation date, fork owner user, fork owner name (if it exists) and fork owner e-mail (if it exists)
-retrieve-all-repos-heuristics is the script for retrieving all the repositories and all their branches, and analyzing using heuristics what each repository is intended for
-analyze-for-ifdefs is the script for analyzing the repositories for commits that contain #ifdef annotations. 

Requirements: 

Python 2.7
Git 1.9.5
MySQL



Usage:

1. Set user and password for MySQL database in retrieve-all-repos-heuristics.py file
2. Then run the retrieve-all-repos-heuristics script.
3. Run analyze-for-ifdefs after the initial script is run (because 2 retrieves all repositories).

Most likely the first script will stop at some point due to forks that may have been removed. 
One should verify if all the forks still exist (modify the script), and then run it. 

To be fixed: first get a list of valid forks in the script that should be used. 