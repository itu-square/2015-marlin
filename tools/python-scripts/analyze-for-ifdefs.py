import subprocess
import os
import csv
import urllib2
import re


a=re.compile("([a-z0-9]){40}")

cfgfile="Configuration.h"
cfgfile1="Configuration_adv.h"
marlinfolder="ErikZalm-Marlin"
deprecated_branches = ['Test','MaukCC','Test_BED','reisnyderb','test2','dob71merge','experimental_zshift','Marlin_os1r1s','Marlin_v1_arduino1.0_test']

def getcommits(branch, repositoryfolder, repo_creation_date):
	return subprocess.Popen(['git','log','--since="'+repo_creation_date+'"','--pretty=oneline'], stderr=subprocess.STDOUT, stdout=subprocess.PIPE,cwd=os.getcwd() + "/" + repositoryfolder).communicate()[0].split("\n")

	# get owner commits which did not modify the Configuration and Configuration_adv files 
def getownercommits(branch, repositoryfolder, repo_creation_date, author):
	command = 'git log --since='+repo_creation_date+' --pretty=oneline --author="'+author+'" -- . ":(exclude)Marlin/Configuration.h" ":(exclude)Marlin/Configuration_adv.h"'
	return subprocess.Popen(command,universal_newlines=True, shell=True,stderr=subprocess.STDOUT, stdout=subprocess.PIPE,cwd=os.getcwd() + "/" + repositoryfolder).communicate()[0].split("\n")


def modifiedfiles(commit,repositoryfolder):
	cmd=subprocess.Popen(['git','diff-tree', '--no-commit-id', '--name-only', '-r', commit],stderr=subprocess.STDOUT, stdout=subprocess.PIPE,cwd=os.getcwd() + "/" + repositoryfolder).communicate()[0].split("\n")
	return cmd
	
def getauthors(repositoryfolder):
	git_authors_cmd = 'git log --all --format="%aN<!--!>%cE"'
	return subprocess.Popen(git_authors_cmd,stderr=subprocess.STDOUT, stdout=subprocess.PIPE,cwd=os.getcwd() + "/" + repositoryfolder).communicate()[0].split("\n")

def setauthor(repo_owner,owner_name, owner_email):
	repo_owner = repo_owner.strip('"')
	owner_name = owner_name.strip('"')
	author= "\("+repo_owner+"\)"
	if owner_name and not owner_name.isspace():
		author= "\("+repo_owner+"\)\|\("+owner_name+"\)"
	if owner_email and not owner_email.isspace():
		author= "\("+repo_owner+"\)\|\("+owner_name+"\)\|\("+owner_email+"\)"
	elif owner_email and not owner_email.isspace():
		author= "\("+repo_owner+"\)\|\("+owner_email+"\)"
					
	return author
	
marlin_commits = getcommits("Marlin_v1",marlinfolder,"2011-08-13T08:07:20Z")	
marlin_authors = set(getauthors(marlinfolder))

resultfile = open('analyze-ifdefs.csv', 'w')
fieldnames = ['repository', 'branch_name','sha']
writer = csv.DictWriter(resultfile, delimiter=',', lineterminator='\n', fieldnames=fieldnames)
writer.writeheader()

	
with open('list_of_forks_and_dates.csv', 'rb') as csvfile:
    repositories = csv.reader(csvfile, delimiter=' ')
    for repo in repositories:
		comma = repo[0].count(',')
		text = ""
		if(comma == 5):
			matches=re.findall(r'\"(.+?)\"',repo[0])
			text = ",".join(matches)
			if(',' in text):
				newtext = text.replace(',',' ')
				repo[0] = repo[0].replace(str(text), str(newtext))

		repo_split = repo[0].split(",", 5)
		repo_split += [None] * (5 - len(repo_split))
		repository, repo_creation_date,repo_owner,owner_name,owner_email = repo_split
	
		print repository
		repo_folder = repository.replace("/","-")
		
		#authors of ErikZalm/Marlin and current repository
		repo_authors = set(getauthors(repo_folder))
		different_authors = repo_authors.difference(marlin_authors)
		
		#Get branches
		remote_branches = subprocess.Popen(["git","branch", "-r"], stderr=subprocess.STDOUT, stdout=subprocess.PIPE,cwd=os.getcwd() + "/" + repo_folder).communicate()[0].split("\n")
		for branch in filter(None,remote_branches):
			branch_name = branch.split("/",1)[-1]
			if branch_name not in deprecated_branches:
				if "HEAD ->" not in branch_name:
					print(branch_name)
					b = subprocess.Popen(['git','checkout',branch_name], cwd=os.getcwd() + "/" + repo_folder,stderr=subprocess.STDOUT, stdout=subprocess.PIPE).communicate()[0]
					#b.wait()
					#get all commits after repository creation date
					
					#construct the regex for commit author - owner of repository, name, e-mail
					author= setauthor(repo_owner,owner_name,owner_email)
					
					# Commits
					commits = getownercommits(branch_name,repo_folder,repo_creation_date,author)
									
					if(len(commits)>1):
						for commit in commits: 
							if(a.match(commit)):
								sha, message = commit.split(" ",1)
								
								# git show sha | grep -i -c "[+][ ]*#ifdef"  - verifies if there were #ifdef additions
								git_show_command = 'git show ' + sha + ' | grep -i -c "[+][ ]*#ifdef"'
								git_show_process = subprocess.Popen(git_show_command,universal_newlines=True, shell=True,stderr=subprocess.STDOUT, stdout=subprocess.PIPE,cwd=os.getcwd() + "/" + repo_folder).communicate()[0].split("\n")
								git_show_result = git_show_process[0]
								if(int(git_show_result) > 0):
									# repository, branch_name, commit
									writer.writerow({'repository':str(repo_folder),'branch_name' : branch_name, 'sha': sha})
									#result.append([repo_folder +","+branch_name+","+str(sha)])
									break
								else:
									continue
#writer.writerows(filter(None,result))					
resultfile.close()	